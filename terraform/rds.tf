resource "aws_db_instance" "demo-db" {
  identifier              = var.rds_identifier
  allocated_storage       = 10
  storage_type            = "gp2"
  engine                  = var.rds_engine
  engine_version          = var.rds_engine_version
  instance_class          = var.rds_instance_class
  parameter_group_name    = var.rds_parameter_group_name
  db_name                 = var.rds_dbname
  username                = var.rds_username
  password                = var.rds_password
  maintenance_window      = var.maintenance_window
  backup_window           = var.backup_window
  backup_retention_period = var.backup_retention_period
  apply_immediately       = false
  skip_final_snapshot     = true
  vpc_security_group_ids  = ["${aws_security_group.rds_sg.id}"]
  db_subnet_group_name    = aws_db_subnet_group.db_subnet.name
  publicly_accessible     = true
}

resource "aws_db_subnet_group" "db_subnet" {
  name       = "main"
  subnet_ids = module.vpc.public_subnets

  tags = {
    Name = "DB subnet group"
  }
}

resource "aws_security_group" "rds_sg" {

  description = "RDS Security Group"

  vpc_id = module.vpc.vpc_id

  ingress {
    description = "MySQL protocol"

    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks     = ["${chomp(data.http.icanhazip.body)}/32"]
    security_groups = ["${aws_security_group.ec2_sg.id}", "${module.lambda_security_group.security_group_id}"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}
