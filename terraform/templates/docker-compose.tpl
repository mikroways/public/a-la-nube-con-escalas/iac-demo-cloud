version: '3.1'

services:

  wordpress-rds:
    image: wordpress
    restart: always
    ports:
      - 8081:80
    environment:
      WORDPRESS_DB_HOST: ${rds_address}
      WORDPRESS_DB_USER: ${rds_username}
      WORDPRESS_DB_PASSWORD: ${rds_password}
      WORDPRESS_DB_NAME: ${rds_dbname}
    volumes:
      - wordpress:/var/www/html

volumes:
  wordpress:
