output "instance_ip" {
  value = aws_instance.iac-demo.public_ip
}

output "rds_host" {
  value = aws_db_instance.demo-db.address
}
