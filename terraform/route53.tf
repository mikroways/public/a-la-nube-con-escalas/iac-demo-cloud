data "aws_route53_zone" "selected" {
  name         = "testing.mikroways.net"
  private_zone = false
}

resource "aws_route53_record" "iac-demo" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "demo-iac.${data.aws_route53_zone.selected.name}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.iac-demo.public_ip]
}
