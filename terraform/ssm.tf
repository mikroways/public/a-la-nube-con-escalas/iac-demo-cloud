resource "aws_ssm_parameter" "sg_id" {
  name        = "/lambda/security_group"
  description = "Lambda security group id"
  type        = "String"
  tier        = "Standard"
  value       = module.lambda_security_group.security_group_id
}

resource "aws_ssm_parameter" "private_subnet" {
  name        = "/lambda/private_subnet"
  description = "Private subnet"
  type        = "String"
  tier        = "Standard"
  value       = module.vpc.private_subnets[0]
}

resource "aws_ssm_parameter" "mysql_host" {
  name        = "/lambda/mysql_host"
  description = "Private subnet"
  type        = "String"
  tier        = "Standard"
  value       = aws_db_instance.demo-db.address
}

resource "aws_ssm_parameter" "mysql_port" {
  name        = "/lambda/mysql_port"
  description = "Private subnet"
  type        = "String"
  tier        = "Standard"
  value       = aws_db_instance.demo-db.port
}

resource "aws_ssm_parameter" "mysql_user" {
  name        = "/lambda/mysql_user"
  description = "Private subnet"
  type        = "String"
  tier        = "Standard"
  value       = var.rds_username
}

resource "aws_ssm_parameter" "mysql_pass" {
  name        = "/lambda/mysql_pass"
  description = "Private subnet"
  type        = "String"
  tier        = "Standard"
  value       = var.rds_password
}

resource "aws_ssm_parameter" "mysql_database" {
  name        = "/lambda/mysql_database"
  description = "Private subnet"
  type        = "String"
  tier        = "Standard"
  value       = var.rds_dbname
}
