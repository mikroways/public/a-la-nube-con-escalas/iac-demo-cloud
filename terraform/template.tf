resource "local_file" "generate_docker_compose_rds" {
  content = templatefile("${path.module}/templates/docker-compose.tpl", {
    rds_address  = aws_db_instance.demo-db.address
    rds_username = var.rds_username
    rds_password = var.rds_password
    rds_dbname   = var.rds_dbname
  })
  filename = "../ansible/files/docker-compose.rds.yaml"
}


resource "local_file" "generate_inventory_file" {
  content = templatefile("${path.module}/templates/hosts.tpl", {
    instance_address = aws_instance.iac-demo.public_ip
  })
  filename = "../ansible/hosts.yml"
}
