const mysql = require('serverless-mysql')({
  config: {
    host     : process.env.MYSQLHOST,
    database : process.env.MYSQLDATABASE,
    user     : process.env.MYSQLUSER,
    password : process.env.MYSQLPASSWORD
  }
})

module.exports = mysql;