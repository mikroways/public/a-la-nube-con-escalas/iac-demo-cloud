const mysql = require("./database");

exports.handler = async (event, context) => {
  // Get users
  let users = await mysql.query('SELECT * FROM wp_users')

  // Run clean up function
  await mysql.end()
 
  // Return the results
  return {
    statusCode: 200,
    body: JSON.stringify(users, null, 2),
  }
}