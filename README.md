# Programando mi infraestructura usando IaC - Demo

Este repositorio contiene el código utilizado para la demostración presentada en la charla "Programando mi infraestructura usando IaC"

## Requerimientos

Para poder trabajar con este repositorio, se debe tener instalado los siguientes requerimientos:

- direnv
- python 3
- direnv
- docker
- docker-compose
- terraform
- ansible
- node 16.13.2

## Ambiente Local

Se despliega un Wordpress y una base de datos utilizando docker-compose.

```console
cd ansible/files
docker-compose up -d
```

Ingresando con el navegador a `localhost:8080` se podrá ver el Wordpress desplegado.

### Generar dumps

El contenedor utilizado como servidor de base de datos es capaz de cargar, al iniciar por primera vez, un dump que inicialice una base de datos. 

Para realizar la carga de este dump, se debe mantener en el directorio **/docker-enmtrypoint-initdb.d** del contenedor de MySQL un archivo **.sql** o **.sql.gz**.

En el repositorio se tiene versionado un archivo dump de ejemplo en el directorio **ansible/files/dump**, por lo que inicialmente se inicializara una base de datos con este Dump.

## Ambiente Remoto

Se desplegará un Wordpress y una base de datos utilizando docker-compose dentro de una instancia EC2. Luego, la base de datos se reemplazará con una instancia de RDS.

Se deben configurar las variables de ambiente `AWS_PROFILE` y `AWS_REGION` modificando el archivo `.envrc` y ejecutando `direnv allow` luego de cada modificación.

### Terraform

Con Terraform se despliega una instancia EC2, un RDS, una VPC con subnets públicas y privadas, un registro en Route 53, registros en Parameter Store y los Security Groups para la instancia EC2, RDS y Lambda.

Para la inicializar el directorio de trabajo:

```console
cd terraform
terraform init
```

Para ver la infraestructura que se desplegará:

```console
cd terraform
terraform plan
```

Para aplicar la infraestructura

```console
cd terraform
terraform apply
```

### Ansible

Con Ansible se aprovisiona la instancia EC2 desplegada con Terraform: se crea un usuario deploy, se instala docker y docker-compose, se copian los archivos necesarios para el funcionamiento de los servicios y se levantan los servicios configurados con docker-compose

```console
cd ansible
pip install -r requirements.txt
ansible-galaxy install -fr requirements.yml && ansible-galaxy collection install -fr requirements.yml
ansible-playbook -i hosts.yml playbook.yml

```

### Wordpress: servidor de bases de datos local

Para correr Wordpress con el servicio de bases de datos local se deben seguir los siguientes pasos:
* Editar una variable de ansible. Esta variable se encuentra en el archivo **ansible/vars.yml**:
    * Se debe modificar la variable **external_db** y el valor que debe tener es **false**.
* Correr nuevamente el playbook de ansible:
    * ```ansible-playbook -i hosts.yml playbook.yml```
* ¿Como ingreso al wordpress? se debe ingresar mediante la direccion IP de la instancia:
    * Ingresar al directorio de terraform, y correr el comando ```terraform output```, la salida **instance_ip** sera la utiliza para acceder al Wordpress desde el navegador.

### Wordpress: servidor de bases de datos como servicio

Para correr Wordpress con el servicio de bases de datos administrado por AWS (AWS RDS) se deben seguir los siguientes pasos:
* Editar una variable de ansible. Esta variable se encuentra en el archivo **ansible/vars.yml**:
    * Se debe modificar la variable **external_db** y el valor que debe tener es **true**.
* Correr nuevamente el playbook de ansible:
    * ```ansible-playbook -i hosts.yml playbook.yml```
* ¿Como ingreso al wordpress? se debe ingresar mediante la direccion IP de la instancia:
    * Ingresar al directorio de terraform, y correr el comando ```terraform output```, la salida **instance_ip** sera la utiliza para acceder al Wordpress desde el navegador.
* El servicio de bases de datos no posee una base de datos con datos, por lo que al ingresar solicitara la instalacion del Wordpress. 
* Opcionalmente, se puede optar por cargar un Dump, de igual manera que se tiene en la opcion **servidor de bases de datos local**.
    * Carga de Dump:
        * Ingresar por SSH a la instancia: ```ssh ubuntu@direccion-ip-instancia```
        * Cargar dump: ```mysql -hinstancia-rds -uuser_admin_rds -p db_name < dump.sql

> Se debe copiar el dump a la instancia, o utilizar el que se sube anteriormente

> Las credenciales de RDS se obtienen desde Terraform    





### Serverless Framework

Con Serverless Framework se deplegará un stack de CloudFormation que contiene una Lambda, un LogGroup de CloudWatch, un Bucket de S3 con el código de la Lambda y un API Gateway. Nuestra función Lambda se conecta con el RDS usado como base de datos del Wordpress desplegado anteriormente y muestra la información de los usuarios almacenados en ella.

#### Local

Utilizando `Serverless Offline`, es posible simular AWS λ y un API Gateway localmente. Para ejecutarlo:

```console
npm i
npm start
```

#### Remoto

Para desplegar remotamente:

```console
npm i
npm run deploy
```

Con el flag `--stage` se puede modificar el ambiente que se desplegará.
Serverless Framework buscará las variables de ambiente correspondientes en el archivo `.env.<stage>`

```console
npm run deploy -- --stage <stage>
```
